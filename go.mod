module gitlab.com/swallow2/swallow-config

go 1.17

require github.com/naoina/toml v0.1.1

require github.com/naoina/go-stringutil v0.1.0 // indirect
