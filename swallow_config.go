package swallow_config

import (
	"fmt"
	"github.com/naoina/toml"
	"os"
	"strconv"
)

type Config struct {
	Flock string `toml:"swallow_flock"`
	Port  string `toml:"swallow_port"`

	EnableSplashScreen bool `toml:"swallow_enable_splash_screen"`

	LogLevel            string `toml:"swallow_log_level"`
	LogFormatter        string `toml:"swallow_log_formatter"`
	LogFormatterDataKey string `toml:"swallow_log_formatter_data_key"`
}

func (c Config) New() Config {
	c.Flock = ""
	c.Port = "9090"

	c.EnableSplashScreen = true

	c.LogLevel = "debug"
	c.LogFormatter = "text"
	c.LogFormatterDataKey = ""

	return c
}

func (c *Config) Load(filepath string) error {
	f, err := os.Open(filepath)
	if err != nil {
		return fmt.Errorf("enalbe to load config file. %v", err)
	}

	defer func(f *os.File) {
		err := f.Close()
		if err != nil {
			panic(err)
		}
	}(f)

	if err := toml.NewDecoder(f).Decode(&c); err != nil {
		return fmt.Errorf("enalbe to parse config file. %v", err)
	}

	return nil
}

func (c *Config) ParseEnv() error {
	str := os.Getenv("SWALLOW_FLOCK")
	if str != "" {
		c.Flock = str
	}

	str = os.Getenv("SWALLOW_PORT")
	if str != "" {
		c.Port = str
	}

	str = os.Getenv("SWALLOW_ENABLE_SPLASH_SCREEN")
	if str != "" {
		EnableSplashScreen, err := strconv.ParseBool(str)
		if err != nil {
			return err
		}

		c.EnableSplashScreen = EnableSplashScreen
	}

	str = os.Getenv("SWALLOW_LOG_LEVEL")
	if str != "" {
		c.LogLevel = str
	}

	str = os.Getenv("SWALLOW_LOG_FORMATTER")
	if str != "" {
		c.LogFormatter = str
	}

	str = os.Getenv("SWALLOW_LOG_FORMATTER_DATA_KEY")
	if str != "" {
		c.LogFormatterDataKey = str
	}

	return nil
}
