package swallow_config

import (
	"os"
	"testing"
)

func assertConfig(c Config, t *testing.T) {
	if c.Flock != "" {
		t.Errorf("c.Flock(%v) != %v", c.Flock, "")
	}

	if c.Port != "9090" {
		t.Errorf("c.Port(%v) != %v", c.Port, "9090")
	}

	if c.EnableSplashScreen != true {
		t.Errorf("c.EnableSplashScreen(%v) != %v", c.EnableSplashScreen, true)
	}

	if c.LogLevel != "debug" {
		t.Errorf("c.LogLevel(%v) != %v", c.LogLevel, "debug")
	}

	if c.LogFormatter != "text" {
		t.Errorf("c.LogFormatter(%v) != %v", c.LogFormatter, "text")
	}

	if c.LogFormatterDataKey != "" {
		t.Errorf("c.LogFormatterDataKey(%v) != %v", c.LogFormatterDataKey, "text")
	}
}

func TestNew(t *testing.T) {
	c := Config{}.New()

	assertConfig(c, t)
}

func TestConfig_Load(t *testing.T) {
	filepath := "/tmp/config.tmp"
	doc := []byte(`
swallow_flock = ""
swallow_port = "9090"
swallow_enable_splash_screen = true
swallow_log_level = "debug"
swallow_log_formatter = "text"
swallow_log_formatter_data_key = ""`)

	err := os.WriteFile(filepath, doc, 0644)
	if err != nil {
		t.Fatal(err)
	}

	c := Config{}

	err = c.Load(filepath)
	if err != nil {
		t.Fatal(err)
	}

	assertConfig(c, t)

	err = os.Remove(filepath)
	if err != nil {
		t.Fatal(err)
	}
}

func TestConfig_ParseEnv(t *testing.T) {
	// set env variables
	err := os.Setenv("SWALLOW_FLOCK", "host1:9090,host2:9090")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Setenv("SWALLOW_PORT", "9090")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Setenv("SWALLOW_ENABLE_SPLASH_SCREEN", "false")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Setenv("SWALLOW_LOG_LEVEL", "warn")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Setenv("SWALLOW_LOG_FORMATTER", "json")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Setenv("SWALLOW_LOG_FORMATTER_DATA_KEY", "data")
	if err != nil {
		t.Fatal(err)
	}

	c := Config{}.New()

	// parse env variable
	err = c.ParseEnv()
	if err != nil {
		t.Fatal(err)
	}

	// assertions
	if c.Flock != "host1:9090,host2:9090" {
		t.Errorf("c.Flock(%v) != %v", c.Flock, "host1:9090,host2:9090")
	}

	if c.Port != "9090" {
		t.Errorf("c.Port(%v) != %v", c.Port, "9090")
	}

	if c.EnableSplashScreen != false {
		t.Errorf("c.EnableSplashScreen(%v) != %v", c.EnableSplashScreen, false)
	}

	if c.LogLevel != "warn" {
		t.Errorf("c.LogLevel(%v) != %v", c.LogLevel, "warn")
	}

	if c.LogFormatter != "json" {
		t.Errorf("c.LogFormatter(%v) != %v", c.LogFormatter, "json")
	}

	if c.LogFormatterDataKey != "data" {
		t.Errorf("c.LogFormatterDataKey(%v) != %v", c.LogFormatterDataKey, "data")
	}

	// unset env variables
	err = os.Unsetenv("SWALLOW_FLOCK")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Unsetenv("SWALLOW_PORT")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Unsetenv("SWALLOW_ENABLE_SPLASH_SCREEN")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Unsetenv("SWALLOW_LOG_LEVEL")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Unsetenv("SWALLOW_LOG_FORMATTER")
	if err != nil {
		t.Fatal(err)
	}

	err = os.Unsetenv("SWALLOW_LOG_FORMATTER_DATA_KEY")
	if err != nil {
		t.Fatal(err)
	}
}
